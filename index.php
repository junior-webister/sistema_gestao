<?php
    include "cabecalho.php";
?>
<style type="text/css">
    body {
      background-color: #F7F7F7;
    }
    .sumir {
        display: none;
    }
</style>
<div class="ui container">
<!--    <div class="ui secondary pointing menu">
      <a class="item active" href="#">
        Geral
      </a>
        <a class="item" href="#">
        Análises
      </a>
        <a class="item" href="#">
        Outros
      </a>
    </div>-->


<?php 
include 'geral.php';
include 'analises.php';
?>

<script>
    var pasta = window.location.pathname;
    var ancora = window.location.hash;
    if (ancora === "#geral" || ancora === "") {
        $('.geral').removeClass('sumir');
        $('#dash').addClass('active');
} else if (ancora === "#analises") {
        $('.analises').removeClass('sumir');
        $('#dash').addClass('active');
}
</script>

<?php
    include "rodape.php";
?>

